### AWS에서 terraform을 이용하여 vpc와 k8s cluster(eks)를 구성하는 코드와 설명

directory는 terraform-test/VPC로 이동한다.

# 1. vpc_v2.tf
하시코프에서 개발한 IaC 개념에서 출발한 자동화 도구인 terraform을 사용하였다.   
우선 클라우드 서버 내에서 논리적인 네트워크 공간을 만들기 위해 vpc를 구성했다.   
1. vpc
2. public subnet 2개
3. private subnet 2개
4. public subnet에 사용될 routing table 2개
5. private subnet에 사용될 routing table 2개
6. public subnet들이 사용할 internet gateway 1개
7. private subnet들이 사용할 nat gateway 1개
![vpc](/uploads/42e3172125681f56262362d022ad49c7/vpc.png)

그리고 vpc의 private subnet에 worker node들을 배포하는 kubernetes cluster를 구축하게 될 eks를 추가했다.   
1. eks module
2. worker group에 마운트될 security group 1개
![vpc_eks](/uploads/f8b12bbf86f8b1ecc21845286b380261/vpc_eks.png)


# 2. instance.tf
private subnet과 유일하게 통신하기 위한 bastion host를 구성했다.   
![bastion_host](/uploads/6c394917ea5fd1c209a92388e2d84aa3/bastion_host.png)

# 3. jenkins-server.tf
CI 파이프라인을 구성하기 위해 jenkins server를 private subnet 내에 설치했다.   

jenkins-server를 위한 ALB는 테라폼을 통해 생성하지 않고 콘솔로 만들기 때문에 코드에는 jenkins-server와 기본 설치 파일을 위한 script 파일들만 있다.   
![jenkins-server](/uploads/e05beb73b65c2d80eb9e84cd0845b360/jenkins-server.png)
